﻿using DeliveryFood.DAL.Contracts;
using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL
{
    public class DishSetRepository : BaseRepository<DishSet, int>, IDishSetRepository
    {
        public DishSetRepository(DeliveryFoodContext dataContext) : base(dataContext) { }
    }
}
