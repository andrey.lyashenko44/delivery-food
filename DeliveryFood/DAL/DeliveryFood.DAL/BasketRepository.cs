﻿using DeliveryFood.DAL.Contracts;
using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL
{
    public class BasketRepository : BaseRepository<Basket, int>, IBasketRepository
    {
        public BasketRepository(DeliveryFoodContext dbContext) : base(dbContext)
        {
        }
    }
}
