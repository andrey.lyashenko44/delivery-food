﻿using DeliveryFood.DAL.Contracts;
using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL
{
    public class ProductsInBasketRepository : BaseRepository<ProductsInBasket, int>, IProductsInBasketRepository
    {
        public ProductsInBasketRepository(DeliveryFoodContext dbContext) : base(dbContext)
        {
        }
    }
}
