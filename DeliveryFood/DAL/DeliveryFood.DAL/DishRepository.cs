﻿using DeliveryFood.DAL.Contracts;
using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL
{
    public class DishRepository : BaseRepository<Dish, int>, IDishRepository
    {
        public DishRepository(DeliveryFoodContext dataContext) : base(dataContext) { }
    }
}
