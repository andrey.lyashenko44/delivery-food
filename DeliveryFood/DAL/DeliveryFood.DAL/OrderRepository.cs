﻿using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;
using Microsoft.EntityFrameworkCore;

namespace DeliveryFood.DAL;

public class OrderRepository : BaseRepository<Order, int>, IOrderRepository
{
    public OrderRepository(DeliveryFoodContext dbContext) : base(dbContext)
    {
    }

    public async Task<IEnumerable<Order>> ListAllAsyncByUser(string userEmail)
    {
        var query = (from order in _dbContext.Orders
                       join basket in _dbContext.Baskets
                       on order.BasketId equals basket.Id
                       where basket.UserBy == userEmail
                       select order);


        return await query.ToListAsync(); 
    }

    public async Task<Order> GetByIdAsyncByUser(int id, string userEmail)
    {
        var query = (from order in _dbContext.Orders
                     join basket in _dbContext.Baskets
                     on order.BasketId equals basket.Id
                     where basket.UserBy == userEmail && order.Id == id
                     select order);

        return await query.FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<Order>> ListAllAsyncByDeliveryman(string userEmail)
    {
        return await _dbContext.Orders.Where(x => x.DeliveryBy == userEmail).ToListAsync();
    }

}