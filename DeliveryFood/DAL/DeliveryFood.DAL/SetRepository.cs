﻿using DeliveryFood.DAL.Contracts;
using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;
using Microsoft.EntityFrameworkCore;

namespace DeliveryFood.DAL
{
    public class SetRepository : BaseRepository<Set, int>, ISetRepository
    {
        public SetRepository(DeliveryFoodContext dataContext) : base(dataContext) { }

        public async Task<Set> GetSetWithDishesAsync(int id)
        {
            return await _dbContext.Sets.Include(set => set.Dishes).SingleOrDefaultAsync(x => x.Id == id);
        }
        public async Task<IEnumerable<Set>> GetAllSetWithDishesAsync()
        {
            return await _dbContext.Sets.Include(set => set.Dishes).ToListAsync();
        }

        public async Task AssigneDishToSet(Set set, Dish dish)
        {
            set.Dishes.Add(dish);
            await _dbContext.SaveChangesAsync();
        }
    }
}
