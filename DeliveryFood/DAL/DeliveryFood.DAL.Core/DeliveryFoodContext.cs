﻿using DeliveryFood.Entities.Core;
using Microsoft.EntityFrameworkCore;

namespace DeliveryFood.DAL.Core
{
    public class DeliveryFoodContext : DbContext
    {
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<DishSet> DishSet { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<ProductsInBasket> ProductsInBaskets { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Set> Sets { get; set; }

        public DeliveryFoodContext(DbContextOptions<DeliveryFoodContext> options)
            : base(options)
        {
            // Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // ConfigureUser(modelBuilder);
            ConfigureDishSet(modelBuilder);
            ConfigureOrderBaskets(modelBuilder);
        }
        
        // private void ConfigureUser(ModelBuilder modelBuilder)
        // {
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.CreatedCategories)
        //         .WithOne(e => e.CreatedBy);
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.ModifiedCategories)
        //         .WithOne(e => e.ModifiedBy);
        //     
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.CreatedDishes)
        //         .WithOne(e => e.CreatedBy);
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.ModifiedDishes)
        //         .WithOne(e => e.ModifiedBy);
        //     
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.CreatedSets)
        //         .WithOne(e => e.CreatedBy);
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.ModifiedSets)
        //         .WithOne(e => e.ModifiedBy);
        //     
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.CreatedOrders)
        //         .WithOne(e => e.CreatedBy);
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.ModifiedOrders)
        //         .WithOne(e => e.ModifiedBy);
        //     modelBuilder
        //         .Entity<User>()
        //         .HasMany(e => e.DeliveryOrders)
        //         .WithOne(e => e.DeliveryBy);
        // }

        private void ConfigureDishSet(ModelBuilder modelBuilder)
        {
            // // custom many-to-many DishSet
            modelBuilder
                .Entity<Dish>()
                .HasMany(c => c.Sets)
                .WithMany(s => s.Dishes)
                .UsingEntity<DishSet>(
                    j => j
                        .HasOne(pt => pt.Set)
                        .WithMany(t => t.DishSets)
                        .HasForeignKey(pt => pt.SetId),
                    j => j
                        .HasOne(pt => pt.Dish)
                        .WithMany(p => p.DishSets)
                        .HasForeignKey(pt => pt.DishId),
                    j =>
                    {
                        j.HasKey(t => new {t.DishId, t.SetId});
                        j.ToTable("DishSet");
                    });
        }
        
        private void ConfigureOrderBaskets(ModelBuilder modelBuilder)
        {
            // custom many-to-many Baskets
            modelBuilder
                .Entity<Set>()
                .HasMany(b => b.Baskets)
                .WithMany(o => o.Sets)
                .UsingEntity<ProductsInBasket>(
                    j => j
                        .HasOne(ob => ob.Basket)
                        .WithMany(b => b.ProductsInBasket)
                        .HasForeignKey(ob => ob.BasketId),
                    j => j
                        .HasOne(ob => ob.Set)
                        .WithMany(o => o.ProductsInBasket)
                        .HasForeignKey(ob => ob.SetId),
                    j =>
                    {
                        j.HasKey(t => new {t.BasketId, t.SetId});
                        j.ToTable("ProductsInBaskets");
                    });
        }
    }
}
