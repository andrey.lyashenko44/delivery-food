namespace DeliveryFood.Entities.Core;

public class Basket : EntityBase
{
    public string? UserBy { get; set; }
    public DateTime DateClosing { get; set; }
    public ICollection<Set> Sets { get; set; } = new List<Set>();
    public ICollection<ProductsInBasket> ProductsInBasket { get; set; } = new List<ProductsInBasket>();
    public ICollection<Order> Orders { get; set; } = new List<Order>();

}