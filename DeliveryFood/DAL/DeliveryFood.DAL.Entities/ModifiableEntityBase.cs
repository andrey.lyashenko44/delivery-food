namespace DeliveryFood.Entities.Core;

public abstract class ModifiableEntityBase : ModifiableEntityWithoutAuthorBase
{
    public string CreatedBy { get; set; }
    public string ModifiedBy { get; set; }
}