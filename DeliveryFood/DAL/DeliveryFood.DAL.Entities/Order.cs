namespace DeliveryFood.Entities.Core;

public class Order : ModifiableEntityBase
{
    public OrderStatus Status { get; set; }
    public int StatusId { get; set; }
    public int SetAmount { get; set; }
    public string Location { get; set; }
    public DateTime TimeInterval { get; set; }
    public string DeliveryBy { get; set; }
    public Basket Basket { get; set; }
    public int BasketId { get; set; }
   
}
