namespace DeliveryFood.Entities.Core;

public abstract class EntityBase
{
    public int Id { get; set; }
}