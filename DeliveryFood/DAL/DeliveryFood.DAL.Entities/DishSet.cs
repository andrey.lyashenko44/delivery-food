namespace DeliveryFood.Entities.Core;

public class DishSet
{
    public Dish Dish { get; set; }
    public int DishId { get; set; }
    public Set Set { get; set; }
    public int SetId { get; set; }
}