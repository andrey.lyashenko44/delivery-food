namespace DeliveryFood.Entities.Core;

public class Set : ModifiableEntityBase
{
    //[Required]
    public string Name { get; set; }
    //[Required]
    public decimal Price { get; set; }
    //[Required]
    public float Weight { get; set; }

    // custom many-to-many
    public ICollection<Dish> Dishes { get; set; } = new List<Dish>();
    public ICollection<DishSet> DishSets { get; set; } = new List<DishSet>();
    public ICollection<Basket> Baskets { get; set; } = new List<Basket>();
    public ICollection<ProductsInBasket> ProductsInBasket { get; set; } = new List<ProductsInBasket>();
}