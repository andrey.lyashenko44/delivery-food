namespace DeliveryFood.Entities.Core;

public class OrderStatus : EntityBase
{
    public string Name { get; set; }
    public ICollection<Order> Orders { get; set; }
}