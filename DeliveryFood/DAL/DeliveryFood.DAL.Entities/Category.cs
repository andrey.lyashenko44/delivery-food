namespace DeliveryFood.Entities.Core;

public class Category : ModifiableEntityBase
{
    public string Name { get; set; }
    // one-to-many
    public ICollection<Dish> Dishes { get; set; } = new List<Dish>();
}