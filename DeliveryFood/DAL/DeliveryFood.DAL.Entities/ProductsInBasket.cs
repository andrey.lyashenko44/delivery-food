namespace DeliveryFood.Entities.Core;

public class ProductsInBasket
{
    public Set Set { get; set; }
    public int SetId { get; set; }
    public Basket Basket { get; set; }
    public int BasketId { get; set; }
}