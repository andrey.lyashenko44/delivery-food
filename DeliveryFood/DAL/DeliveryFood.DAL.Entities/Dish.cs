﻿namespace DeliveryFood.Entities.Core
{
    public class Dish : ModifiableEntityBase
    {
        public string Name { get; set; }

        // one-to-many
        public Category Category { get; set; }
        public int CategoryId { get; set; }
        public bool IsActive { get; set; }

        // custom many-to-many
        public ICollection<Set> Sets { get; set; } = new List<Set>();
        public ICollection<DishSet> DishSets { get; set; } = new List<DishSet>();
    }
}
