// namespace DeliveryFood.Entities.Core;
//
// public class User : ModifiableEntityWithoutAuthorBase
// {
//     public string Email { get; set; }
//     public string FirstName { get; set; }
//     public string LastName { get; set; }
//     public Role Role { get; set; }
//     public bool IsActive { get; set; }
//     
//     public ICollection<Category> CreatedCategories { get; set; }
//     public ICollection<Category> ModifiedCategories { get; set; }
//     
//     public ICollection<Dish> CreatedDishes { get; set; }
//     public ICollection<Dish> ModifiedDishes { get; set; }
//     
//     public ICollection<Set> CreatedSets { get; set; }
//     public ICollection<Set> ModifiedSets { get; set; }
//     
//     public ICollection<Order> DeliveryOrders { get; set; }
//     public ICollection<Order> CreatedOrders { get; set; }
//     public ICollection<Order> ModifiedOrders { get; set; }
// }