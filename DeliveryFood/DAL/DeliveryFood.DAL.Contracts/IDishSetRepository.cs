﻿using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL.Contracts;

public interface IDishSetRepository : IRepository<DishSet, int>, IAsyncRepository<DishSet, int>
{

}