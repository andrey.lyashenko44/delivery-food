﻿using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL.Contracts
{
    public interface ICategoryRepository : IRepository<Category, int>, IAsyncRepository<Category, int>
    {

    }
}
