﻿using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL.Contracts
{
    public interface ISetRepository : IRepository<Set, int>, IAsyncRepository<Set, int>
    {
        public Task<Set> GetSetWithDishesAsync(int id);
        public Task<IEnumerable<Set>> GetAllSetWithDishesAsync();
        public Task AssigneDishToSet(Set set, Dish dish);
    }
}
