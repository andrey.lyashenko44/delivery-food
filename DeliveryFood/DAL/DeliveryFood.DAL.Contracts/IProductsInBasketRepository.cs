﻿using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL.Contracts
{
    public interface IProductsInBasketRepository : IRepository<ProductsInBasket, int>, IAsyncRepository<ProductsInBasket, int>
    {

    }
}
