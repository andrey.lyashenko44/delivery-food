﻿using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL.Contracts;

public interface IOrderRepository : IRepository<Order, int>, IAsyncRepository<Order, int>
{
    public Task<Order> GetByIdAsyncByUser(int id, string userEmail);
    public Task<IEnumerable<Order>> ListAllAsyncByUser(string userEmail);

    public Task<IEnumerable<Order>> ListAllAsyncByDeliveryman(string userEmail);
    
}