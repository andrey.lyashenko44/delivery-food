﻿using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL.Contracts
{
    public interface IBasketRepository : IRepository<Basket, int>, IAsyncRepository<Basket, int>
    {
        
    }
}
