using DeliveryFood.DAL.Core;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.DAL.Contracts;

public interface IDishRepository : IRepository<Dish, int>, IAsyncRepository<Dish, int>
{
        
}


