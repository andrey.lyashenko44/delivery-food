﻿using AutoMapper;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.BLL;

public class OrderService : IOrderService
{
    private readonly IMapper _mapper;

    private readonly IOrderRepository _orderRepository;
    private readonly IBasketRepository _basketRepository;
    
    public OrderService(IOrderRepository orderRepository, IMapper mapper, IBasketRepository basketRepository)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _basketRepository = basketRepository;
    }

    public async Task<AddOrderDTO> AddOrderAsync(AddOrderDTO addOrderDTO, int basketId)
    {
        var order = _mapper.Map<Order>(addOrderDTO);

        order.CreatedBy = "User";
        order.ModifiedBy = "User";
        order.CreatedDate = DateTime.UtcNow;
        order.ModifiedDate = order.CreatedDate;
        order.BasketId = basketId;
        
        await _orderRepository.AddAsync(order);

        return addOrderDTO;
    }

    public async Task<GetSpecificOrderDTO> GetSpecificOrderAsync(int id, string userEmail)
    {
        var getSpecificOrder = await _orderRepository.GetByIdAsyncByUser(id, userEmail);

        var getSpecificOrderDTO = _mapper.Map<GetSpecificOrderDTO>(getSpecificOrder);

        return getSpecificOrderDTO;
    }

    public async Task<IEnumerable<GetAllOrdersSpecificDTO>> GetAllOrdersSpecificAsync(string userEmail)
    {
        var listAllOrderSpecific = await _orderRepository.ListAllAsyncByUser(userEmail);

        var listAllOrderSpecificDTO = listAllOrderSpecific.Select(order => _mapper.Map<GetAllOrdersSpecificDTO>(order));
        return listAllOrderSpecificDTO;
    }
    
    public async Task<IEnumerable<GetAllOrdersDTO>> GetAllOrdersAsync()
    {
        var listAllOrder = await _orderRepository.ListAllAsync();

        var listAllOrderDTO = _mapper.Map<IEnumerable<GetAllOrdersDTO>>(listAllOrder);

        return listAllOrderDTO;
    }

    public async Task<GetOrderDTO> GetOrderAsyncById(int id)
    {
        var getOrder = await _orderRepository.GetByIdAsync(id);

        var getOrderDTO = _mapper.Map<GetOrderDTO>(getOrder);

        return getOrderDTO;
    }

    public async Task<UpdateOrderStatusDTO> UpdateOrderStatus(UpdateOrderStatusDTO updateOrderStatusDTO)
    {
        var orderStatus = _mapper.Map<Order>(updateOrderStatusDTO);

        await _orderRepository.UpdateAsync(orderStatus);
        return updateOrderStatusDTO;
    }

    public async Task<UpdateDeliveryIdDTO> UpdateDeliveryId(UpdateDeliveryIdDTO updateDeliveryIdDTO)
    {
        var updateDeliveryId = _mapper.Map<Order>(updateDeliveryIdDTO);

        await _orderRepository.UpdateAsync(updateDeliveryId);
        return updateDeliveryIdDTO;
    }

    public async Task<IEnumerable<GetAllOrdersForSpecificDeliverymanDTO>> GetAllOrdersSpecificDeliverymanAsync(string userEmail)
    {

        var listAllOrderSpecificDeliveryman = await _orderRepository.ListAllAsyncByDeliveryman(userEmail);

        var listAllOrderSpecificDTO = _mapper.Map<IEnumerable<GetAllOrdersForSpecificDeliverymanDTO>>(listAllOrderSpecificDeliveryman);

        return listAllOrderSpecificDTO;

    }

    public async Task<UpdateOrderStatusDeliverymanDTO> UpdateOrderStatusDeliverymanAsync(UpdateOrderStatusDeliverymanDTO updateOrderStatusDeliverymanDTO)
    {

        var isOrder = _orderRepository.GetByIdAsync(updateOrderStatusDeliverymanDTO.Id);

        if (isOrder is not null)
        {

            var order = _mapper.Map<Order>(updateOrderStatusDeliverymanDTO);

            order.StatusId = 2;

            await _orderRepository.UpdateAsync(order);

            return updateOrderStatusDeliverymanDTO;
        }
        throw new NullReferenceException("Данный заказ отсутствует в базе");

    }
}
