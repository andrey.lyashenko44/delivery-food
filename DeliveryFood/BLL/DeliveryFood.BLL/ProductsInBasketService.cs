﻿using AutoMapper;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.BLL
{
    public class ProductsInBasketService : IProductsInBasketService
    {
        private readonly IProductsInBasketRepository _productsInBasketRepository;
        private readonly IMapper _mapper;

        public ProductsInBasketService(IProductsInBasketRepository productsInBasketRepository, IMapper mapper)
        {
            _productsInBasketRepository = productsInBasketRepository;
            _mapper = mapper;
        }

        public async Task<AddProductsInBasketDTO> AddProductsInBasketAsync(AddProductsInBasketDTO addProductsInBasketDTO)
        {
            var productsInBasket = _mapper.Map<ProductsInBasket>(addProductsInBasketDTO);

            await _productsInBasketRepository.AddAsync(productsInBasket);

            return addProductsInBasketDTO;
        }

        public async Task<GetProductsInBasketDTO> GetProductsInBasketAsync(int id)
        {
            var getProductsInBasket = await _productsInBasketRepository.GetByIdAsync(id);

            var getProductsInBasketDTO = _mapper.Map<GetProductsInBasketDTO>(getProductsInBasket);

            return getProductsInBasketDTO;
        }

        public async Task<RemoveProductsInBasketDTO> RemoveProductsInBasketAsync(RemoveProductsInBasketDTO removeProductsInBasketDTO)
        {
            var removeProductsInBasket = _mapper.Map<ProductsInBasket>(removeProductsInBasketDTO);

            await _productsInBasketRepository.DeleteAsync(removeProductsInBasket);

            return removeProductsInBasketDTO;
        }

        public async Task<UpdateProductsInBasketDTO> UpdateProductsInBasket(UpdateProductsInBasketDTO updateProductsInBasketDTO)
        {
            var updateProductsInBasket = _mapper.Map<ProductsInBasket>(updateProductsInBasketDTO);

            await _productsInBasketRepository.UpdateAsync(updateProductsInBasket);

            return updateProductsInBasketDTO;
        }
    }
}
