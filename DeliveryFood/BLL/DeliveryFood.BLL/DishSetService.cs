﻿using AutoMapper;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.BLL
{
    public class DishSetService: IDishSetService
    {
        private readonly IDishSetRepository _dishSetRepository;
        private readonly IMapper _mapper;

        public DishSetService(IDishSetRepository dishSetRepository, IMapper mapper)
        {
            _dishSetRepository = dishSetRepository;
            _mapper = mapper;
        }

        public async Task<AddDishSetDTO> AddDishSetAsync(AddDishSetDTO addNewDishSetDTO)
        {
            var dishSet = _mapper.Map<DishSet>(addNewDishSetDTO);

            await _dishSetRepository.AddAsync(dishSet);

            return addNewDishSetDTO;
        }

        public async Task<RemoveDishSetDTO> RemoveDishSetAsync(RemoveDishSetDTO removeDishSetDTO)
        {
            var getRemoveDishSet = _mapper.Map<DishSet>(removeDishSetDTO);

            await _dishSetRepository.DeleteAsync(getRemoveDishSet);

            return removeDishSetDTO;
        }

        public async Task<IEnumerable<GetAllDishSetsDTO>> GetAllDishSetsAsync()
        {
            var listAlldish = await _dishSetRepository.ListAllAsync();

            var listAllDishSetDTO = _mapper.Map<IEnumerable<GetAllDishSetsDTO>>(listAlldish);

            return listAllDishSetDTO;
        }

    }
}
