﻿using AutoMapper;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.BLL
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryService(ICategoryRepository categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<AddCategoryDTO> AddCategoryAsync(AddCategoryDTO addCategoryDTO)
        {
            
            var category = _mapper.Map<Category>(addCategoryDTO);
            category.CreatedBy = "Admin";
            category.ModifiedBy = "Admin";
            category.CreatedDate = DateTime.UtcNow;
            category.ModifiedDate = DateTime.UtcNow;

            await _categoryRepository.AddAsync(category);

            return addCategoryDTO;
        }

        public async Task<GetCategoryDTO> GetCategoryAsync(int id)
        {
            var getCategory = await _categoryRepository.GetByIdAsync(id);

            var getCategoryDTO = _mapper.Map<GetCategoryDTO>(getCategory);

            return getCategoryDTO;
        }

        public async Task<RemoveCategoryDTO> RemoveCategoryAsync(RemoveCategoryDTO removeCategoryDTO)
        {
            var removeCategory = _mapper.Map<Category>(removeCategoryDTO);

            await _categoryRepository.DeleteAsync(removeCategory);

            return removeCategoryDTO;
        }

        public async Task<UpdateCategoryDTO> UpdateCategory(UpdateCategoryDTO updateCategoryDTO)
        {
            var updateCategory = _mapper.Map<Category>(updateCategoryDTO);

            await _categoryRepository.UpdateAsync(updateCategory);

            return updateCategoryDTO;
        }
    }
}
