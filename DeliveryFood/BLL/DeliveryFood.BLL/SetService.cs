﻿using AutoMapper;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.BLL;

public class SetService : ISetService
{
    private readonly ISetRepository _setRepository;

    private readonly IMapper _mapper;

    private readonly IDishRepository _dishRepository;

    public SetService(ISetRepository setRepository, IMapper mapper, IDishRepository dishRepository)
    {
        _setRepository = setRepository;
        _mapper = mapper;
        _dishRepository = dishRepository;
    }

    public async Task<AddSetDTO> AddSetAsync(AddSetDTO addSetDTO)
    {
        var set = _mapper.Map<Set>(addSetDTO);

        // todo заменить на почту из токена
        set.CreatedBy = "email";
        if (set.CreatedBy == null) throw new NullReferenceException("User not found");
        set.ModifiedBy = set.CreatedBy;
        set.CreatedDate = DateTime.UtcNow;
        set.ModifiedDate = set.CreatedDate;

        await _setRepository.AddAsync(set);

        return addSetDTO;
    }

    public async Task<RemoveSetDTO> RemoveSetAsync(RemoveSetDTO removeSetDTO)
    {
        // var getRemoveSet = _mapper.Map<Set>(removeSetDTO);
        var getRemoveSet = await _setRepository.GetByIdAsync(removeSetDTO.Id);
        
        
        await _setRepository.DeleteAsync(getRemoveSet);

        return removeSetDTO;
    }

    public async Task<UpdateSetDTO> UpdateSet(UpdateSetDTO updateSetDTO)
    {
        var set = await _setRepository.GetByIdAsync(updateSetDTO.Id);
        _mapper.Map(updateSetDTO, set);
        await _setRepository.UpdateAsync(set);
        return updateSetDTO;
    }

    public async Task<SetWithAssigneeDishesDTO> GetSetWithAssigneeDishes(int id)
    {
        var set = await _setRepository.GetSetWithDishesAsync(id);
        var setWithAssigneeDishesDTO = _mapper.Map<SetWithAssigneeDishesDTO>(set);
        return setWithAssigneeDishesDTO;
    }

    public async Task<IEnumerable<SetWithAssigneeDishesDTO>> GetAllSetWithAssigneeDishes()
    {
        var setList = await _setRepository.GetAllSetWithDishesAsync();
        var setListWithAssigneeDishesDTO = _mapper.Map<IEnumerable<SetWithAssigneeDishesDTO>>(setList);
        return setListWithAssigneeDishesDTO;
    }

    public async Task AssigneDishesToSet(int SetId, params int[] dishId)
    {
        var set = await _setRepository.GetByIdAsync(SetId);
        if (set == null) throw new NullReferenceException("Set not found");
        foreach (var item in dishId)
        {
            var dish = _dishRepository.GetById(item);
            if (dish == null) throw new NullReferenceException("Dish not found");
            await _setRepository.AssigneDishToSet(set, dish);
        }
    }
}