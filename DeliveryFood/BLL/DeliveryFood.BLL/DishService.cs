﻿using AutoMapper;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.BLL;

public class DishService : IDishService
{
    private readonly ICategoryRepository _categoryRepository;
    private readonly IDishRepository _dishRepository;
    private readonly IMapper _mapper;

    public DishService(IDishRepository dishRepository, IMapper mapper, ICategoryRepository categoryRepository)
    {
        _dishRepository = dishRepository;
        _mapper = mapper;
        _categoryRepository = categoryRepository;
    }

    public async Task<AddDishDTO> AddDishAsync(AddDishDTO addNewDishDTO)
    {
        var isCategory = await _categoryRepository.GetByIdAsync(addNewDishDTO.CategoryId);


        if (isCategory is not null)
        {
            var dish = _mapper.Map<Dish>(addNewDishDTO);

            dish.CreatedBy = "Admin";
            //TODO: переделать контроллеры и сервисы после пересмотрения архитерктуры БД. Как вариант вместо email передавать GUID 

            dish.ModifiedBy = "Admin";
            dish.CreatedDate = DateTime.UtcNow;
            dish.ModifiedDate = dish.CreatedDate;

            await _dishRepository.AddAsync(dish);

            return addNewDishDTO;
        }

        throw new NullReferenceException("Данная категория отсутствует в базе");
    }

    public async Task<IEnumerable<GetAllDishesDTO>> GetAllDishAsync()
    {
        var listAlldish = await _dishRepository.ListAllAsync();

        var listAllDishDTO = _mapper.Map<IEnumerable<GetAllDishesDTO>>(listAlldish);

        return listAllDishDTO;
    }

    public async Task<GetDishDTO> GetDishAsync(int id)
    {
        var getDish = await _dishRepository.GetByIdAsync(id);

        var getDishDTO = _mapper.Map<GetDishDTO>(getDish);

        return getDishDTO;
    }

    public async Task<RemoveDishDTO> RemoveDishAsync(RemoveDishDTO removeDishDTO)
    {
        var dish = _dishRepository.GetById(removeDishDTO.Id);
        await _dishRepository.DeleteAsync(dish);

        return removeDishDTO;
    }

    public async Task<UpdateDishDTO> UpdateDish(UpdateDishDTO updateDishDTO)
    {
        var isCategory = await _categoryRepository.GetByIdAsync(updateDishDTO.CategoryId);

        if (isCategory is not null)
        {
            var dish = _dishRepository.GetById(updateDishDTO.Id);
            _mapper.Map(updateDishDTO, dish);

            //dish.ModifiedBy = user;
            //TODO: переделать после согласования структуры БД

            dish.ModifiedDate = DateTime.UtcNow;

            await _dishRepository.UpdateAsync(dish);

            return updateDishDTO;
        }

        throw new NullReferenceException("Данная категория отсутствует в базе");
    }
}