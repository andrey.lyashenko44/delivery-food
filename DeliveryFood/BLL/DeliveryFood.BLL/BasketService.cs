﻿using AutoMapper;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.BLL
{
    public class BasketService : IBasketService
    {
        private readonly IBasketRepository _basketRepository;
        private readonly IMapper _mapper;

        public BasketService(IBasketRepository basketRepository, IMapper mapper)
        {
            _basketRepository = basketRepository;
            _mapper = mapper;
        }

        public async Task<AddBasketDTO> AddBasketAsync(AddBasketDTO addBasketDTO)
        {
            var basket = _mapper.Map<Basket>(addBasketDTO);

            await _basketRepository.AddAsync(basket);

            return addBasketDTO;
        }

        public async Task<GetBasketDTO> GetBasketAsync(int id)
        {
            var getBasket = await _basketRepository.GetByIdAsync(id);

            var getBasketDTO = _mapper.Map<GetBasketDTO>(getBasket);

            return getBasketDTO;
        }

        public async Task<RemoveBasketDTO> RemoveBasketAsync(RemoveBasketDTO removeBasketDTO)
        {
            var removeBasket = _mapper.Map<Basket>(removeBasketDTO);

            await _basketRepository.DeleteAsync(removeBasket);

            return removeBasketDTO;
        }

        public async Task<UpdateBasketDTO> UpdateBasket(UpdateBasketDTO updateBasketDTO)
        {
            var updateBasket = _mapper.Map<Basket>(updateBasketDTO);

            await _basketRepository.UpdateAsync(updateBasket);

            return updateBasketDTO;
        }
    }
}
