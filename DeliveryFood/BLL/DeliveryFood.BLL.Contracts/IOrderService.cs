﻿using DeliveryFood.BLL.DTO;

namespace DeliveryFood.BLL.Contracts;

public interface IOrderService
{
    Task<AddOrderDTO> AddOrderAsync(AddOrderDTO addOrderDTO, int basketId);
    Task<GetSpecificOrderDTO> GetSpecificOrderAsync(int id, string userEmail);
    Task<IEnumerable<GetAllOrdersSpecificDTO>> GetAllOrdersSpecificAsync(string userEmail);
    Task<GetOrderDTO> GetOrderAsyncById(int id);
    Task<IEnumerable<GetAllOrdersDTO>> GetAllOrdersAsync();
    Task<UpdateOrderStatusDTO> UpdateOrderStatus(UpdateOrderStatusDTO updateOrderStatusDTO);
    Task<UpdateDeliveryIdDTO> UpdateDeliveryId(UpdateDeliveryIdDTO updateDeliveryIdDTO);
    Task<IEnumerable<GetAllOrdersForSpecificDeliverymanDTO>> GetAllOrdersSpecificDeliverymanAsync(string userEmail);

    Task<UpdateOrderStatusDeliverymanDTO> UpdateOrderStatusDeliverymanAsync(
        UpdateOrderStatusDeliverymanDTO updateOrderStatusDeliverymanDTO);
}