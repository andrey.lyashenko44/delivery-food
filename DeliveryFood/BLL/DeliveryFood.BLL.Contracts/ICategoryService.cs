﻿using DeliveryFood.BLL.DTO;

namespace DeliveryFood.BLL.Contracts;

public interface ICategoryService
{
    Task<AddCategoryDTO> AddCategoryAsync(AddCategoryDTO addCategoryDTO);
    Task<RemoveCategoryDTO> RemoveCategoryAsync(RemoveCategoryDTO removeCategoryDTO);
    Task<GetCategoryDTO> GetCategoryAsync(int id);
    Task<UpdateCategoryDTO> UpdateCategory(UpdateCategoryDTO updateCategoryDTO);
}