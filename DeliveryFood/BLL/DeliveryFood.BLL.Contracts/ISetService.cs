using DeliveryFood.BLL.DTO;

namespace DeliveryFood.BLL.Contracts;

public interface ISetService
{
    Task<AddSetDTO> AddSetAsync(AddSetDTO addSetDTO);
    Task<RemoveSetDTO> RemoveSetAsync(RemoveSetDTO removeSetDTO);
    Task<UpdateSetDTO> UpdateSet(UpdateSetDTO updateSetDTO);
    Task<SetWithAssigneeDishesDTO> GetSetWithAssigneeDishes(int id);
    Task<IEnumerable<SetWithAssigneeDishesDTO>> GetAllSetWithAssigneeDishes();
    Task AssigneDishesToSet(int SetId, params int[] dishId);
}