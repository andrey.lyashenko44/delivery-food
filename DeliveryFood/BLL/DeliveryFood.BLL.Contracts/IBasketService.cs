﻿using DeliveryFood.BLL.DTO;

namespace DeliveryFood.BLL.Contracts;

public interface IBasketService
{
    Task<AddBasketDTO> AddBasketAsync(AddBasketDTO addBasketDTO);
    Task<RemoveBasketDTO> RemoveBasketAsync(RemoveBasketDTO removeBasketDTO);
    Task<GetBasketDTO> GetBasketAsync(int id);
    Task<UpdateBasketDTO> UpdateBasket(UpdateBasketDTO updateBasketDTO);
}