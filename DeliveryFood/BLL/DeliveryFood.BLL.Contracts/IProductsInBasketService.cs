﻿using DeliveryFood.BLL.DTO;

namespace DeliveryFood.BLL.Contracts;

public interface IProductsInBasketService
{
    Task<AddProductsInBasketDTO> AddProductsInBasketAsync(AddProductsInBasketDTO addProductsInBasketDTO);
    Task<RemoveProductsInBasketDTO> RemoveProductsInBasketAsync(RemoveProductsInBasketDTO removeProductsInBasketDTO);
    Task<GetProductsInBasketDTO> GetProductsInBasketAsync(int id);
    Task<UpdateProductsInBasketDTO> UpdateProductsInBasket(UpdateProductsInBasketDTO updateProductsInBasketDTO);
}