﻿using DeliveryFood.BLL.DTO;

namespace DeliveryFood.BLL.Contracts;

public interface IDishSetService
{
    Task<AddDishSetDTO> AddDishSetAsync(AddDishSetDTO addDishSetDTO);
    Task<RemoveDishSetDTO> RemoveDishSetAsync(RemoveDishSetDTO removeDishSetDTO);
    Task<IEnumerable<GetAllDishSetsDTO>> GetAllDishSetsAsync();
}