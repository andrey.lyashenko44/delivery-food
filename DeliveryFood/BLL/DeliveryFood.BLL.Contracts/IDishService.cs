using DeliveryFood.BLL.DTO;

namespace DeliveryFood.BLL.Contracts;

public interface IDishService
{
    Task<AddDishDTO> AddDishAsync(AddDishDTO addDishDTO);
    Task<RemoveDishDTO> RemoveDishAsync(RemoveDishDTO removeDishDTO);
    Task<GetDishDTO> GetDishAsync(int id);
    Task<IEnumerable<GetAllDishesDTO>> GetAllDishAsync();
    Task<UpdateDishDTO> UpdateDish(UpdateDishDTO updateDishDTO);
}