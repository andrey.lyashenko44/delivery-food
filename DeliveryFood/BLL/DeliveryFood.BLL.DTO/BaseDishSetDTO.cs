﻿namespace DeliveryFood.BLL.DTO
{
    public class BaseDishSetDTO
    {
        public int DishId { get; set; }
        public int SetId { get; set; }
    }
}
