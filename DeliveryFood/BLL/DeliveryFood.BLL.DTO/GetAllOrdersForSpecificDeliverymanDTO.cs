﻿namespace DeliveryFood.BLL.DTO
{
    public class GetAllOrdersForSpecificDeliverymanDTO : BaseOrderDTO
    {
        public int Id { get; set; }

    }
}
