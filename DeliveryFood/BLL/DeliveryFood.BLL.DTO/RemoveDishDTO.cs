﻿namespace DeliveryFood.BLL.DTO;

public class RemoveDishDTO: BaseDishDTO
{
    public int Id { get; set; }
    
}

