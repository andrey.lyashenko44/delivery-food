﻿namespace DeliveryFood.BLL.DTO
{
    public class GetSpecificOrderDTO: BaseOrderDTO
    {
        public int Id { get; set; }
    }
}
