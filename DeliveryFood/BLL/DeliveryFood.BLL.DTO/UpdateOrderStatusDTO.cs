﻿namespace DeliveryFood.BLL.DTO;

public class UpdateOrderStatusDTO
{
    public int Id { get; set; }
    public int StatusId { get; set; }

}

