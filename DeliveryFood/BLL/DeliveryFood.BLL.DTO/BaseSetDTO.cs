﻿namespace DeliveryFood.BLL.DTO;

public abstract class BaseSetDTO
{
    public string Name { get; set; }
    public decimal Price { get; set; }
    public float Weight { get; set; }

}
