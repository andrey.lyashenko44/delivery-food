﻿namespace DeliveryFood.BLL.DTO
{
    public class GetBasketDTO : BaseBasketDTO
    {
        public int Id { get; set; }
    }
}
