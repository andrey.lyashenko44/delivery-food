﻿namespace DeliveryFood.BLL.DTO
{
    public class GetProductsInBasketDTO : BaseProductsInBasket
    {
        public int Id { get; set; }
    }
}
