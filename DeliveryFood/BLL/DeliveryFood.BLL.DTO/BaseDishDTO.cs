﻿namespace DeliveryFood.BLL.DTO;

public abstract class BaseDishDTO
{
    public string Name { get; set; }
    // one-to-many
    public int CategoryId { get; set; }
    public bool IsActive { get; set; }

}
