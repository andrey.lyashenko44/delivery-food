﻿namespace DeliveryFood.BLL.DTO
{
    public class GetAllOrdersSpecificDTO : BaseOrderDTO
    {
        public int Id { get; set; }
    }
}
