﻿namespace DeliveryFood.BLL.DTO
{
    public class RemoveBasketDTO : BaseBasketDTO
    {
        public int Id { get; set; }
    }
}
