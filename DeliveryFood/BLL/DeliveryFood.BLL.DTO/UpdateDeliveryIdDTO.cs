﻿namespace DeliveryFood.BLL.DTO
{
    public class UpdateDeliveryIdDTO
    {
        public int Id { get; set; }
        public int DeliveryId { get; set; }
    }
}
