﻿namespace DeliveryFood.BLL.DTO
{
    public class RemoveSetDTO : BaseSetDTO
    {
        public int Id { get; set; }

    }
}
