﻿namespace DeliveryFood.BLL.DTO
{
    public class RemoveProductsInBasketDTO : BaseProductsInBasket
    {
        public int Id { get; set; }
    }
}
