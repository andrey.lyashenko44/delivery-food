﻿namespace DeliveryFood.BLL.DTO
{
    public class UpdateBasketDTO : BaseBasketDTO
    {
        public int Id { get; set; }
    }
}
