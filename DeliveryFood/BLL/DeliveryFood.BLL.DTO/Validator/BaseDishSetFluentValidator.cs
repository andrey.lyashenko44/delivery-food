﻿using DeliveryFood.Entities.Core;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validation;

public class BaseDishSetDTOFluentValidator : AbstractValidator<BaseDishSetDTO>
{

    public BaseDishSetDTOFluentValidator()
    {
        RuleFor(x => x.DishId)
            .NotNull()
            .NotEmpty()
            .WithMessage("ID блюда не может быть пустым!");

        RuleFor(x => x.SetId)
            .NotNull()
            .NotEmpty()
            .WithMessage("ID сета не может быть пустым!");

    }

}
