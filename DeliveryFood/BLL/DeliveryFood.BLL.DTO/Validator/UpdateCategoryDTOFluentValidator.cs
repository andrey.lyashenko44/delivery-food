﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class UpdateCategoryDTOFluentValidator : AbstractValidator<UpdateCategoryDTO>
    {

        public UpdateCategoryDTOFluentValidator()
        {
            Include(new BaseCategoryDTOFluentValidator());

            RuleFor(x => x.Id)
                    .NotNull().WithMessage("Идентификатор корзины не может быть пустым!");
        }

    }
}
