﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class AddDishDTOFluentValidator : AbstractValidator<AddDishDTO>
    {
        public AddDishDTOFluentValidator()
        {
            Include(new BaseDishDTOFluentValidator());       
        }

    }
}
