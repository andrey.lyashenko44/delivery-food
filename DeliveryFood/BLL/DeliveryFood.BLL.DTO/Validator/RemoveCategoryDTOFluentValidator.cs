﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class RemoveCategoryDTOFluentValidator : AbstractValidator<RemoveCategoryDTO>
    {

        public RemoveCategoryDTOFluentValidator()
        {
            Include(new BaseCategoryDTOFluentValidator());

            RuleFor(x => x.Id)
                    .NotNull().WithMessage("Идентификатор категории не может быть пустым!");
        }

    }
}
