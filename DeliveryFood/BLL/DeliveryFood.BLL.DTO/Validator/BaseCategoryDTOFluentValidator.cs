﻿using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class BaseCategoryDTOFluentValidator : AbstractValidator<BaseCategoryDTO>
    {
        public BaseCategoryDTOFluentValidator()
        {
            RuleFor(x => x.Name)
            .NotNull()
            .NotEmpty()
            .WithMessage("The name of the food category cannot be empty!");
            // .WithMessage("Название категории блюд не может быть пустым!");

            RuleForEach(x => x.Name).Must(Char.IsLetter).WithMessage("The name should contain only letters");
        }

    }
}