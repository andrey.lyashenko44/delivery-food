﻿using DeliveryFood.Entities.Core;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validation;

public class BaseSetDTOFluentValidator : AbstractValidator<BaseSetDTO>
{

    public BaseSetDTOFluentValidator()
    {
        RuleFor(x => x.Name)
            .NotNull()
            .NotEmpty()
            .WithMessage("Название сета не может быть пустым!");

        RuleForEach(x => x.Name).
            Where(x => x != ' ').
            Cascade(CascadeMode.Stop).
            Must(Char.IsLetter).
            WithMessage("В названии должны быть только буквы");

        RuleFor(x => x.Price)
            .NotEmpty()
            .WithMessage("Цена не может быть пустой");

        RuleFor(x => x.Price)
            .GreaterThan(0)
            .WithMessage("Цена меньше 0. С таким подходом недолго стать банкротом");

        RuleFor(x => x.Weight)
            .NotEmpty()
            .WithMessage("Вес не может быть пустой");

        RuleFor(x => x.Weight)
            .GreaterThan(0)
            .WithMessage("Вес меньше 0. Гравитационные волны только открыли, а вы уже изменяете вес предметов");

    }

}
