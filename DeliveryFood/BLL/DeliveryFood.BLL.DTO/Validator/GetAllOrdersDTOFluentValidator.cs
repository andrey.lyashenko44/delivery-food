﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetAllOrdersDTOFluentValidator : AbstractValidator<GetAllOrdersDTO>
    {

        public GetAllOrdersDTOFluentValidator()
        {
            Include(new BaseOrderDTOFluentValidator());

            RuleFor(x => x.Id)
                .NotNull().WithMessage("Идентификатор заказов не может быть пустым!");
        }

    }
}
