﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetAllOrderForSpecificDeliverymanDTOFluentValidator : AbstractValidator<GetAllOrdersForSpecificDeliverymanDTO>
    {

        public GetAllOrderForSpecificDeliverymanDTOFluentValidator()
        {
            Include(new BaseOrderDTOFluentValidator());

            RuleFor(x => x.Id)
                .NotNull().WithMessage("Идентификатор заказов для доставщика не может быть пустым!");
        }

    }
}
