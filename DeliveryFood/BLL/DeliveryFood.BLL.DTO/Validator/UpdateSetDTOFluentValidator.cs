﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class UpdateSetDTOFluentValidator : AbstractValidator<UpdateSetDTO>
    {
        public UpdateSetDTOFluentValidator()
        {
            Include(new BaseSetDTOFluentValidator());

            RuleFor(x => x.Id)
            .NotNull().WithMessage("Идентификатор сета не может быть пустой!");
        }

    }
}
