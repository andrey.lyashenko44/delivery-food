﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class AddDishSetDTOFluentValidator : AbstractValidator<AddDishSetDTO>
    {
        public AddDishSetDTOFluentValidator()
        {
            Include(new BaseDishSetDTOFluentValidator());
        }

    }
}
