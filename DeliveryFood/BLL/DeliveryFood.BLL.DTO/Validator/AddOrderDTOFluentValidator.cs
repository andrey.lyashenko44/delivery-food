﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class AddOrderDTOFluentValidator : AbstractValidator<AddOrderDTO>
    {
        public AddOrderDTOFluentValidator()
        {
            Include(new BaseOrderDTOFluentValidator());
        }

    }
}
