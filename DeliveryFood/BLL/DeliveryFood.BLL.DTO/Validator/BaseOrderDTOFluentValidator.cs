﻿using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validation;

public class BaseOrderDTOFluentValidator : AbstractValidator<BaseOrderDTO>
{

    public BaseOrderDTOFluentValidator()
    {
        RuleFor(x => x.StatusId)
            .NotNull().WithMessage("Состояние заказа не может быть пустым");
        RuleFor(x => x.SetAmount)
            .NotNull().WithMessage("Поле не может быть пустым");
        RuleFor(x => x.TimeInterval)
            .NotNull().WithMessage("Укажите время доставки");
        //RuleFor(x => x.Location)
        //    .NotEmpty().WithMessage("Укажите адрес доставки")
        //    .Must(x => IsValidLocation(x)).WithMessage("Проверьте правильность ввода адреса, пример: Санкт-Петербург, Невский пр.80, кв. 175");

    }

    private bool IsValidLocation(string location)
    {
        throw new NotImplementedException();// TODO: Предложить добавить сервис для работы с адресами DaData https://dadata.ru/api/clean/address/
    }
}
