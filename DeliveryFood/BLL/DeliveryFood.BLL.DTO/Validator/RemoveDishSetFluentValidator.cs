﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class RemoveDishSetDTOFluentValidator : AbstractValidator<RemoveDishSetDTO>
    {
        public RemoveDishSetDTOFluentValidator()
        {
            Include(new BaseDishSetDTOFluentValidator());

            // RuleFor(x => x.Id)
            // .NotNull().WithMessage("Идентификатор списка блюд не может быть пустой!");
        }

    }
}
