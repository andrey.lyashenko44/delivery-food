﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetSpecificOrderDTOFluentValidator : AbstractValidator<GetSpecificOrderDTO>
    {

        public GetSpecificOrderDTOFluentValidator()
        {
            Include(new BaseOrderDTOFluentValidator());

            RuleFor(x => x.Id)
                .NotNull().WithMessage("Идентификатор особого заказа не может быть пустым!");
        }

    }
}
