﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class RemoveBasketDTOFluentValidator : AbstractValidator<RemoveBasketDTO>
    {

        public RemoveBasketDTOFluentValidator()
        {
            Include(new BaseBasketDTOFluentValidator());

            RuleFor(x => x.Id)
                    .NotNull().NotEmpty().WithMessage("Идентификатор корзины не может быть пустым!");
        }

    }
}
