﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetAllOrdersSpecificDTOFluentValidator : AbstractValidator<GetAllOrdersSpecificDTO>
    {

        public GetAllOrdersSpecificDTOFluentValidator()
        {
            Include(new BaseOrderDTOFluentValidator());

            RuleFor(x => x.Id)
                .NotNull().WithMessage("Идентификатор списка особых заказов не может быть пустым!");
        }

    }
}
