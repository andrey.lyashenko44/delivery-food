﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetOrderDTOFluentValidator : AbstractValidator<GetOrderDTO>
    {

        public GetOrderDTOFluentValidator()
        {
            Include(new BaseOrderDTOFluentValidator());

            RuleFor(x => x.Id)
                    .NotNull().WithMessage("Идентификатор заказа не может быть пустым!");
        }

    }
}
