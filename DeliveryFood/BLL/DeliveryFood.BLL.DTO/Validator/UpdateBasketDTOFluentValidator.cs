﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class UpdateBasketDTOFluentValidator : AbstractValidator<UpdateBasketDTO>
    {

        public UpdateBasketDTOFluentValidator()
        {
            Include(new BaseBasketDTOFluentValidator());

            RuleFor(x => x.Id)
                    .NotNull().WithMessage("Идентификатор корзины не может быть пустым!");
        }

    }
}
