﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetAllDishSetsDTOFluentValidator : AbstractValidator<GetAllDishSetsDTO>
    {

        public GetAllDishSetsDTOFluentValidator()
        {
            Include(new BaseDishSetDTOFluentValidator());

            // RuleFor(x => x.Id)
                    // .NotNull().WithMessage("Идентификатор набора блюд не может быть пустым!");
        }

    }
}
