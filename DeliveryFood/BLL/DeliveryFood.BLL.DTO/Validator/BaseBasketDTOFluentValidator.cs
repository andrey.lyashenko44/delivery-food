﻿using DeliveryFood.Entities.Core;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validation;

public class BaseBasketDTOFluentValidator : AbstractValidator<BaseBasketDTO>
{

    public BaseBasketDTOFluentValidator()
    {
        RuleFor(x => x.UserBy)
            .NotNull()
            .NotEmpty()
            .WithMessage("Пользователь не может быть пустым!");

    }

}
