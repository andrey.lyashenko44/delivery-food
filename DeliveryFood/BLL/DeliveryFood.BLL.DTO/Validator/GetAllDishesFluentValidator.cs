﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetAllDishesDTOFluentValidator : AbstractValidator<GetAllDishesDTO>
    {

        public GetAllDishesDTOFluentValidator()
        {
            Include(new BaseDishDTOFluentValidator());

            RuleFor(x => x.Id)
                    .NotNull().WithMessage("Идентификатор списка блюд не может быть пустым!");
        }

    }
}
