﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class AddBasketDTOFluentValidator : AbstractValidator<AddBasketDTO>
    {

        public AddBasketDTOFluentValidator()
        {
            Include(new BaseBasketDTOFluentValidator());

        }

    }
}
