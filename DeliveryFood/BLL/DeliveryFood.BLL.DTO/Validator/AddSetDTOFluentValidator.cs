﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class AddSetDTOFluentValidator : AbstractValidator<AddSetDTO>
    {
        public AddSetDTOFluentValidator()
        {
            Include(new BaseSetDTOFluentValidator());
        }

    }
}
