﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class GetDishDTOFluentValidator : AbstractValidator<GetDishDTO>
    {

        public GetDishDTOFluentValidator()
        {
            Include(new BaseDishDTOFluentValidator());

            RuleFor(x => x.Id)
                    .NotNull().WithMessage("Идентификатор блюда не может быть пустым!");
        }

    }
}
