﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class RemoveDishDTOFluentValidator: AbstractValidator<RemoveDishDTO>
    {
        public RemoveDishDTOFluentValidator()
        {
            Include(new BaseDishDTOFluentValidator());

            RuleFor(x => x.Id)
            .NotNull().WithMessage("Идентификатор блюда не может быть пустой!");
        }

    }
}
