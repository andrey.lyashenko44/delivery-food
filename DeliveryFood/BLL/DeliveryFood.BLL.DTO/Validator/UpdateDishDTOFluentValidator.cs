﻿using DeliveryFood.BLL.DTO.Validation;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validator
{
    public class UpdateDishDTOFluentValidator : AbstractValidator<UpdateDishDTO>
    {
        public UpdateDishDTOFluentValidator()
        {
            Include(new BaseDishDTOFluentValidator());

            RuleFor(x => x.Id)
            .NotNull().WithMessage("Идентификатор блюда не может быть пустой!");
        }

    }
}
