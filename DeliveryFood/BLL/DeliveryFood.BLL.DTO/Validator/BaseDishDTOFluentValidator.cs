﻿using DeliveryFood.Entities.Core;
using FluentValidation;

namespace DeliveryFood.BLL.DTO.Validation;

public class BaseDishDTOFluentValidator : AbstractValidator<BaseDishDTO>
{

    public BaseDishDTOFluentValidator()
    {
        RuleFor(x => x.Name)
            .NotEmpty().WithMessage("Название блюда не может быть пустым!");

        RuleForEach(x => x.Name).
            Where(x => x != ' ').
            Cascade(CascadeMode.Stop).
            Must(Char.IsLetter).
            WithMessage("В названии должны быть только буквы");

        RuleFor(x => x.CategoryId)
            .NotNull().WithMessage("Блюду должна быть назначена категория!");

        RuleFor(x => x.IsActive)
            .NotNull().WithMessage("Доступность блюда не должна быть пустой!");


    }

}
