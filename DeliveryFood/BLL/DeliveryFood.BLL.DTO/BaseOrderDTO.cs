﻿namespace DeliveryFood.BLL.DTO;

public abstract class BaseOrderDTO
{
    public int StatusId { get; set; }
    public int SetAmount { get; set; }
    public string Location { get; set; }
    public DateTime TimeInterval { get; set; }
    public string DeliveryBy { get; set; }

}
