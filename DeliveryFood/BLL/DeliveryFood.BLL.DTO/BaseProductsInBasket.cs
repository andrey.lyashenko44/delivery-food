﻿namespace DeliveryFood.BLL.DTO
{
    public class BaseProductsInBasket
    {
        public int SetId { get; set; }
        public int BasketId { get; set; }
    }
}
