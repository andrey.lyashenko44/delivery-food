﻿namespace DeliveryFood.BLL.DTO;

public class UpdateOrderStatusDeliverymanDTO
{
    public int Id { get; set; }
    public int StatusId { get; set; }

}
