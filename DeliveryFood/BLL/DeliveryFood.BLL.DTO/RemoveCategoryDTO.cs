﻿namespace DeliveryFood.BLL.DTO;

public class RemoveCategoryDTO: BaseCategoryDTO
{
    public int Id { get; set; }
    
}
