﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryFood.BLL.DTO
{
    public abstract class BaseCategoryDTO
    {
        public string Name { get; set; }
    }
}
