﻿using DeliveryFood.Web.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Text.Json;

namespace DeliveryFood.Web.Middlewares
{
    // public class ExceptionHandling : ControllerBase
    // {
    //     private readonly RequestDelegate _requestDelegate;
    //     private readonly ILogger<ExceptionHandling> _logger;
    //
    //     public ExceptionHandling(RequestDelegate requestDelegate, ILogger<ExceptionHandling> logger)
    //     {
    //         _requestDelegate = requestDelegate;
    //         _logger = logger;
    //     }
    //
    //     public async Task InvokeAsync(HttpContext httpContext)
    //     {
    //         try
    //         {
    //             await _requestDelegate(httpContext);
    //         }
    //         catch (NullReferenceException ex)
    //         {
    //             await HandleExceptionAsync(httpContext, ex.Message, HttpStatusCode.BadRequest, "Неправильное значение в базе!");
    //
    //         }
    //         catch (Exception ex)
    //         {
    //             await HandleExceptionAsync(httpContext, ex.Message, HttpStatusCode.InternalServerError, "Произошла ошибка при обработке данных!");
    //             
    //         }
    //         
    //     }
    //
    //
    //     private async Task HandleExceptionAsync(HttpContext httpContext, string messageexception, HttpStatusCode httpStatusCode, string ourmessage)
    //     {
    //         _logger.LogError(messageexception, httpStatusCode);
    //
    //         HttpResponse response = httpContext.Response;
    //         if (response != null)
    //         {
    //             response.ContentType = "application/json";
    //             response.StatusCode = (int)httpStatusCode;
    //
    //             ErrorDTO errorDTO = new()
    //             {
    //                 Message = ourmessage,
    //                 StatusCode = (int)httpStatusCode,
    //
    //             };
    //             
    //             await response.WriteAsJsonAsync(errorDTO);
    //
    //         }
    //     }
    // }
}
