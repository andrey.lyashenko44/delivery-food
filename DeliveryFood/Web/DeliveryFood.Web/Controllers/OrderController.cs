﻿using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFood.Web.Controllers;

[Route("api/[controller]/[action]")]
[Produces("application/json")]
[ApiController]
public class OrderController : ControllerBase
{
    private readonly IOrderService _orderService;

    public OrderController(IOrderService orderService)
    {
        _orderService = orderService;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<AddOrderDTO>> AddOrder(AddOrderDTO addOrderDTO, int basketId)
    {
        if (ModelState.IsValid)
        {
            var orderDTO = await _orderService.AddOrderAsync(addOrderDTO, basketId);
            return Ok(orderDTO);
        }

        var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
            .SelectMany(e => e.Errors)
            .Select(e => e.ErrorMessage)
            .ToList();
        return BadRequest(validationErrors);
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<GetSpecificOrderDTO>> GetOrderSpecificByEmail(int id, string userEmail)
    {
       
            var getOrderDTO = await _orderService.GetSpecificOrderAsync(id, userEmail);
            return Ok(getOrderDTO);
        
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<GetAllOrdersSpecificDTO>>> GetAllOrdersSpecificByEmail(string userEmail)
    {
       
            var listOrderDTO = await _orderService.GetAllOrdersSpecificAsync(userEmail);
            return Ok(listOrderDTO);
       
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<GetAllOrdersDTO>>> GetAllOrders()
    {
        var listOrderDTO = await _orderService.GetAllOrdersAsync();
        return Ok(listOrderDTO);
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<GetOrderDTO>> GetById(int id)
    {
        var getOrderDTO = await _orderService.GetOrderAsyncById(id);
        return Ok(getOrderDTO);
    }

    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<UpdateOrderStatusDTO>> UpdateOrderStatus(UpdateOrderStatusDTO updateOrderStatusDTO)
    {
        var result = await _orderService.UpdateOrderStatus(updateOrderStatusDTO);
        return Ok(result);
    }

    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<UpdateDeliveryIdDTO>> UpdateDeliveryId(UpdateDeliveryIdDTO updateDeliveryIdDTO)
    {
        var result = await _orderService.UpdateDeliveryId(updateDeliveryIdDTO);
        return Ok(result);
    }

    #region MethodsOrdersDeliveryman

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<GetAllOrdersForSpecificDeliverymanDTO>>> GetAllOrdersSpecificDeliveryman(string userEmail)
    {
            if (ModelState.IsValid)
            {
                var listOrderDTO = await _orderService.GetAllOrdersSpecificDeliverymanAsync(userEmail);
                return Ok(listOrderDTO);
            }
            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);

    }

    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<UpdateOrderStatusDeliverymanDTO>> UpdateOrderStatusDeliveryman(UpdateOrderStatusDeliverymanDTO updateOrderStatusDeliverymanDTO)
    {
        var result = await _orderService.UpdateOrderStatusDeliverymanAsync(updateOrderStatusDeliverymanDTO);
        return Ok(result);
    }



    #endregion

}
