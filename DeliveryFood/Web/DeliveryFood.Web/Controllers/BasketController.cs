﻿using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFood.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        private readonly IBasketService _basketService;

        public BasketController(IBasketService basketService)
        {
            _basketService = basketService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<AddBasketDTO>> AddBasket(AddBasketDTO addBasketDTO)
        {
            if (ModelState.IsValid)
            {
                var basketDTO = await _basketService.AddBasketAsync(addBasketDTO);

                return Ok(basketDTO);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);

        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GetBasketDTO>> GetById(int id)
        {
            var getBasketDTO = await _basketService.GetBasketAsync(id);

            return Ok(getBasketDTO);
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<RemoveBasketDTO>> RemoveBasket(RemoveBasketDTO removeBasketDTO)
        {
            if (ModelState.IsValid)
            {
                var result = await _basketService.RemoveBasketAsync(removeBasketDTO);

                return Ok(result);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<UpdateBasketDTO>> UpdateBasket(UpdateBasketDTO updateBasketDTO)
        {
            if (ModelState.IsValid)
            {
                var result = await _basketService.UpdateBasket(updateBasketDTO);
                return Ok(result);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);
        }

    }
}
