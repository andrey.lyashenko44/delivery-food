﻿using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFood.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<AddCategoryDTO>> AddCategory(AddCategoryDTO addCategoryDTO)
        {
            if (ModelState.IsValid)
            {
                var categoryDTO = await _categoryService.AddCategoryAsync(addCategoryDTO);

                return Ok(categoryDTO);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            
            return BadRequest(validationErrors.FirstOrDefault());

        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GetCategoryDTO>> GetById(int id)
        {
            var getCategoryDTO = await _categoryService.GetCategoryAsync(id);

            return Ok(getCategoryDTO);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<RemoveCategoryDTO>> RemoveCategory([FromQuery] RemoveCategoryDTO removeCategoryDTO)
        {
            if (ModelState.IsValid)
            {
                var result = await _categoryService.RemoveCategoryAsync(removeCategoryDTO);

                return Ok(result);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);

        }

    }
}
