﻿using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFood.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [Produces("application/json")]
    [ApiController]
    public class SetController : ControllerBase
    {
        private readonly ISetService _setService;

        public SetController(ISetService setService)
        {
            _setService = setService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<AddSetDTO>> AddSet(AddSetDTO addSetDTO)
        {
            if (ModelState.IsValid)
            {
                var setDTO = await _setService.AddSetAsync(addSetDTO);

                return Ok(setDTO);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);

        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<RemoveSetDTO>> RemoveSet(RemoveSetDTO removeSetDTO)
        {
            if (ModelState.IsValid)
            {
                var result = await _setService.RemoveSetAsync(removeSetDTO);

                return Ok(result);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);

        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<UpdateSetDTO>> UpdateSet(UpdateSetDTO updateSetDTO)
        {
            if (ModelState.IsValid)
            {
                var result = await _setService.UpdateSet(updateSetDTO);

                return Ok(result);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);

        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<SetWithAssigneeDishesDTO>> SetWithAssigneeDishes(int id)
        {
            var result = await _setService.GetSetWithAssigneeDishes(id);
            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<SetWithAssigneeDishesDTO>> AllSetsWithAssigneeDishes()
        {
            var result = await _setService.GetAllSetWithAssigneeDishes();
            return Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> AssigneDishesToSet(int SetId, params int[] dishId)
        {
            await _setService.AssigneDishesToSet(SetId, dishId);
            return Ok();
        }
    }
}
