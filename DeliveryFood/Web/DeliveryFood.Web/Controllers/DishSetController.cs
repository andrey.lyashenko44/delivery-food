﻿using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFood.Web.Controllers;

[Route("api/[controller]/[action]")]
[Produces("application/json")]
[ApiController]

public class DishSetController : ControllerBase
{

    private readonly IDishSetService _dishSetService;

    public DishSetController(IDishSetService dishSetService)
    {
        _dishSetService = dishSetService;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<AddDishSetDTO>> AddDishSet(AddDishSetDTO addDishSetDTO)
    {
        if (ModelState.IsValid)
        {
            var dishSetDTO = await _dishSetService.AddDishSetAsync(addDishSetDTO);

            return Ok(dishSetDTO);
        }

        var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
            .SelectMany(e => e.Errors)
            .Select(e => e.ErrorMessage)
            .ToList();
        return BadRequest(validationErrors);

    }

    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<RemoveDishSetDTO>> RemoveDishSet(RemoveDishSetDTO removeDishSetDTO)
    {
        if (ModelState.IsValid)
        {
            var result = await _dishSetService.RemoveDishSetAsync(removeDishSetDTO);

            return Ok(result);
        }

        var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
            .SelectMany(e => e.Errors)
            .Select(e => e.ErrorMessage)
            .ToList();
        return BadRequest(validationErrors);

    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<GetAllDishSetsDTO>>> GetAllDishSets()
    {
        var listDishSetDTO = await _dishSetService.GetAllDishSetsAsync();

        return Ok(listDishSetDTO);
    }

}

