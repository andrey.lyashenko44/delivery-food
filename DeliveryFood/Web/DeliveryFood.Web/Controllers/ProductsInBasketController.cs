﻿using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFood.Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductsInBasketController : ControllerBase
    {
        private readonly IProductsInBasketService _productsInBasketService;

        public ProductsInBasketController(IProductsInBasketService productsInBasketService)
        {
            _productsInBasketService = productsInBasketService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<AddProductsInBasketDTO>> AddProductsInBasket(AddProductsInBasketDTO addProductsInBasketDTO)
        {
            var productsInBasketDTO = await _productsInBasketService.AddProductsInBasketAsync(addProductsInBasketDTO);

            return Ok(productsInBasketDTO);

        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GetProductsInBasketDTO>> GetById(int id)
        {
            var getProductsInBasketDTO = await _productsInBasketService.GetProductsInBasketAsync(id);

            return Ok(getProductsInBasketDTO);
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<RemoveProductsInBasketDTO>> RemoveProductsInBasket(RemoveProductsInBasketDTO removeProductsInBasketDTO)
        {
            var result = await _productsInBasketService.RemoveProductsInBasketAsync(removeProductsInBasketDTO);

            return Ok(result);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<UpdateProductsInBasketDTO>> UpdateProductsInBasket(UpdateProductsInBasketDTO updateProductsInBasketDTO)
        {
            var result = await _productsInBasketService.UpdateProductsInBasket(updateProductsInBasketDTO);
            return Ok(result);
        }

    }
}
