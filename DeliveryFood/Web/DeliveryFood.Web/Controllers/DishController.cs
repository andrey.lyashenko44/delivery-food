using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryFood.Web.Controllers;

[Route("api/[controller]/[action]")]
[Produces("application/json")]
[ApiController]
public class DishController : ControllerBase
{
    private readonly IDishService _dishService;

    public DishController(IDishService dishService)
    {
        _dishService = dishService;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<AddDishDTO>> AddDish(AddDishDTO addDishDTO)
    {
            if (ModelState.IsValid)
            {
                var dishDTO = await _dishService.AddDishAsync(addDishDTO);

                return Ok(dishDTO);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                .SelectMany(e => e.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();
            return BadRequest(validationErrors);
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<GetDishDTO>> GetById(int id)
    {
        var getDishDTO = await _dishService.GetDishAsync(id);

        return Ok(getDishDTO);
    }

    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<RemoveDishDTO>> RemoveDish(RemoveDishDTO removeDishDTO)
    {
            if (ModelState.IsValid)
            {

                var result = await _dishService.RemoveDishAsync(removeDishDTO);

                return Ok(result);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                    .SelectMany(e => e.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
            return BadRequest(validationErrors);
    }
    
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<GetAllDishesDTO>>> GetAllDishes()
    {
        var listDishDTO = await _dishService.GetAllDishAsync();

        return Ok(listDishDTO);
    }

    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<UpdateDishDTO>> UpdateDish(UpdateDishDTO updateDishDTO)
    {
            if (ModelState.IsValid)
            {

                var result = await _dishService.UpdateDish(updateDishDTO);
                return Ok(result);
            }

            var validationErrors = ModelState.Values.Where(e => e.Errors.Count > 0)
                    .SelectMany(e => e.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
            return BadRequest(validationErrors);
    }
}
