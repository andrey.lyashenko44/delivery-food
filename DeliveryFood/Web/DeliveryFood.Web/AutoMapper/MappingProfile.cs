using AutoMapper;
using DeliveryFood.BLL.DTO;
using DeliveryFood.Entities.Core;

namespace DeliveryFood.Web.AutoMapper;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Dish, UpdateDishDTO>().ReverseMap();
        CreateMap<Dish, AddDishDTO>().ReverseMap();
        CreateMap<Dish, RemoveDishDTO>().ReverseMap();
        CreateMap<Dish, GetDishDTO>().ReverseMap();
        CreateMap<Dish, GetAllDishesDTO>().ReverseMap();

        CreateMap<Set, AddSetDTO>().ReverseMap();

        CreateMap<Category, UpdateCategoryDTO>().ReverseMap();
        CreateMap<Category, AddCategoryDTO>().ReverseMap();
        CreateMap<Category, RemoveCategoryDTO>().ReverseMap();
        CreateMap<Category, GetCategoryDTO>().ReverseMap();
       

        CreateMap<DishSet, AddDishSetDTO>().ReverseMap();
        CreateMap<DishSet, RemoveDishSetDTO>().ReverseMap();
        CreateMap<DishSet, GetAllDishSetsDTO>().ReverseMap();

        CreateMap<Set, AddSetDTO>().ReverseMap();
        CreateMap<Set, UpdateSetDTO>().ReverseMap();
        CreateMap<Set, RemoveSetDTO>().ReverseMap();
        CreateMap<Set, SetWithAssigneeDishesDTO>().ReverseMap();
        CreateMap<Dish, DishDTO>().ReverseMap();

        CreateMap<Order, GetOrderDTO>().ReverseMap();
        CreateMap<Order, GetAllOrdersDTO>().ReverseMap();
        CreateMap<Order, UpdateOrderStatusDTO>().ReverseMap();
        CreateMap<Order, UpdateDeliveryIdDTO>().ReverseMap();
        CreateMap<Order, AddOrderDTO>().ReverseMap();
        CreateMap<Order, GetSpecificOrderDTO>().ReverseMap();
        CreateMap<Order, GetAllOrdersSpecificDTO>().ReverseMap();
        CreateMap<Order, GetAllOrdersForSpecificDeliverymanDTO>().ReverseMap();

        CreateMap<Basket, AddBasketDTO>().ReverseMap();
        CreateMap<Basket, RemoveBasketDTO>().ReverseMap();
        CreateMap<Basket, UpdateBasketDTO>().ReverseMap();
        CreateMap<Basket, GetBasketDTO>().ReverseMap();

        CreateMap<ProductsInBasket, AddProductsInBasketDTO>().ReverseMap();
        CreateMap<ProductsInBasket, RemoveProductsInBasketDTO>().ReverseMap();
        CreateMap<ProductsInBasket, UpdateProductsInBasketDTO>().ReverseMap();
        CreateMap<ProductsInBasket, GetProductsInBasketDTO>().ReverseMap();

    }
}