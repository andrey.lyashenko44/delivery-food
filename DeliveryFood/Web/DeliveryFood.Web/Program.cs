using DeliveryFood.BLL;
using DeliveryFood.BLL.Contracts;
using DeliveryFood.BLL.DTO.Validation;
using DeliveryFood.DAL;
using DeliveryFood.DAL.Contracts;
using DeliveryFood.DAL.Core;
using DeliveryFood.Web.Middlewares;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAutoMapper(typeof(Program));

#region custom DI
string connection = builder.Configuration.GetConnectionString("DeliveryFoodConnection");
builder.Services.AddDbContext<DeliveryFoodContext>(options => options.UseNpgsql(connection));

builder.Services.AddFluentValidation(fv =>
{
    fv.RegisterValidatorsFromAssemblyContaining<BaseDishDTOFluentValidator>();
    fv.DisableDataAnnotationsValidation = true;
});

builder.Services.AddScoped<IDishRepository, DishRepository>();
builder.Services.AddScoped<IDishService, DishService>();

builder.Services.AddScoped<ISetRepository, SetRepository>();
builder.Services.AddScoped<ISetService, SetService>();

builder.Services.AddScoped<ICategoryRepository, CategoryRepository>();
builder.Services.AddScoped<ICategoryService, CategoryService>();

builder.Services.AddScoped<IDishSetRepository, DishSetRepository>();
builder.Services.AddScoped<IDishSetService, DishSetService>();

builder.Services.AddScoped<IOrderRepository, OrderRepository>();
builder.Services.AddScoped<IOrderService, OrderService>();

builder.Services.AddScoped<IBasketRepository, BasketRepository>();
builder.Services.AddScoped<IBasketService, BasketService>();

builder.Services.AddScoped<IProductsInBasketRepository, ProductsInBasketRepository>();
builder.Services.AddScoped<IProductsInBasketService, ProductsInBasketService>();

#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// app.UseMiddleware<ExceptionHandling>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
