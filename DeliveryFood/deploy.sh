docker build -t mopin/delivery-food -f ./Web/DeliveryFood.Web/Dockerfile .
docker push  mopin/delivery-food
ssh otus <<'ENDSSH'
    cd /home/delivery-food/
    docker pull mopin/delivery-food
    docker-compose up -d
ENDSSH
